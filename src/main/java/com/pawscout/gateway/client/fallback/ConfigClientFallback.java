package com.pawscout.gateway.client.fallback;

import com.pawscout.common.dto.config.MinimalLegalVersion;
import com.pawscout.gateway.client.ConfigClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ConfigClientFallback implements ConfigClient {

    private final Logger logger = LoggerFactory.getLogger("feign");

    @Override
    public MinimalLegalVersion getMinimalLegalVersionVersion(String authorizationToken) {
        logger.error("Problems with connection to config client service");
        return null;
    }
}

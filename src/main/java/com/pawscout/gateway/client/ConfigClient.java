package com.pawscout.gateway.client;

import com.pawscout.common.dto.config.MinimalLegalVersion;
import com.pawscout.gateway.client.fallback.ConfigClientFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "CONFIG", fallback = ConfigClientFallback.class)
public interface ConfigClient {

    @RequestMapping(method = RequestMethod.GET, value = "/api/legalVersion")
    MinimalLegalVersion getMinimalLegalVersionVersion(@RequestHeader("Authorization") String authorizationToken);
}

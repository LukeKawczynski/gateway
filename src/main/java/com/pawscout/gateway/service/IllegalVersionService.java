package com.pawscout.gateway.service;

import com.pawscout.common.dto.config.MinimalLegalVersion;
import com.pawscout.common.security.jwt.TokenProvider;
import com.pawscout.gateway.client.ConfigClient;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.nonNull;

@Slf4j
@Service
public class IllegalVersionService {

    private static final String PAWSCOUT_USER_AGENT_PREFIX = "Pawscout";
    private static final String PAWSCOUT_ANDROID_USER_AGENT = "Pawscout-Android";
    private static final String PAWSCOUT_IOS_USER_AGENT = "Pawscout-iOS";
    private final MinVersion minVersion;
    private final ConfigClient configClient;
    private final Set<String> userAgentCache = new HashSet<>();
    private TokenProvider tokenProvider;

    @Autowired
    public IllegalVersionService(ConfigClient configClient, TokenProvider tokenProvider) {
        this.configClient = configClient;
        this.tokenProvider = tokenProvider;
        this.minVersion = new MinVersion();
        //updateMinVersion();
    }

    @Scheduled(fixedRate = 300000, initialDelay = 5000)
    public void updateMinVersionScheduler() {
        updateMinVersion();
    }

    public boolean isValid(String userAgent) {
        if (!userAgent.startsWith(PAWSCOUT_USER_AGENT_PREFIX)) {
            return true;
        }
        String[] osAndVersion = userAgent.split(" ")[0].split("/");
        if (osAndVersion[0].equals(PAWSCOUT_ANDROID_USER_AGENT)) {
            return isVersionForAndroidIsLegal(Long.valueOf(osAndVersion[1].split("-")[1]));
        } else if (osAndVersion[0].equals(PAWSCOUT_IOS_USER_AGENT)) {
            return isVersionForIosIsLegal(osAndVersion[1]);
        }

        return false;
    }

    private void updateMinVersion() {
        MinimalLegalVersion minimalLegalVersion = configClient.getMinimalLegalVersionVersion(
            "Bearer " + tokenProvider.createServiceAccessToken());


        if (nonNull(minimalLegalVersion) && (!minimalLegalVersion.getAndroidMinVersion()
            .equals(minVersion.getAndroidMinVersion()) ||
                                             !minimalLegalVersion.getIosMinVersion()
                                                 .equals(minVersion.getIosMinVersion()))) {
            log.info("Minimal version updated: {}", minimalLegalVersion);

            userAgentCache.clear();

            minVersion.setAndroidMinVersion(minimalLegalVersion.getAndroidMinVersion());
            minVersion.setIosMinVersion(minimalLegalVersion.getIosMinVersion());
        }
    }

    private boolean isVersionForAndroidIsLegal(Long version) {
        if (nonNull(minVersion.getAndroidMinVersion())) {
            return minVersion.getAndroidMinVersion() <= version;
        }
        return true;
    }

    private boolean isVersionForIosIsLegal(String iosVersion) {
        if (nonNull(minVersion.getIosMinVersion())) {
            return minVersion.getIosMinVersion()
                       .compareToIgnoreCase(iosVersion.split("-")[0]) <= 0;
        }
        return true;
    }

    @Getter
    @Setter
    private class MinVersion {

        private String iosMinVersion;

        private Long androidMinVersion;
    }
}

package com.pawscout.gateway.gateway.logger;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.pawscout.common.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import static java.util.Objects.nonNull;

@Component
public class LoggerFilter extends ZuulFilter {
    private static final String USER_AUTH_ENDPOINT = "/user/api/auth";
    private static final String HEADER_USER_AGENT = "User-Agent";
    private final Logger log = LoggerFactory.getLogger(LoggerFilter.class);

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 101;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        String userId = SecurityUtils.getCurrentUserIdWithoutThrowingException();
        InputStream in = (InputStream) ctx.get("requestEntity");
        String userAgent = ctx.getRequest()
            .getHeader(HEADER_USER_AGENT);
        if (in == null) {
            try {
                in = ctx.getRequest()
                    .getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String requestBody = getRequestBody(in);
        String requestURI = ctx.getRequest()
            .getRequestURI();
        if (requestURI.startsWith("/resource")) {
            requestBody = "";
        } else {
            requestBody = hideCredentials(requestBody);
        }
        log.info("TESTTEST:{}, {}", ctx.getRequest()
            .getHeader("X-FORWARDED-FOR"), ctx.getRequest()
            .getRemoteAddr());
        log.info("Ip:{},UserId:{}, Url:{} {}, requestParams:{} , ResponseCode:{}, userAgent:{}, body:{}",
            ctx.getRequest()
                .getHeader("X-FORWARDED-FOR"), userId, ctx.getRequest()
                .getMethod(), requestURI, nonNull(ctx.getRequestQueryParams()) ? ctx.getRequestQueryParams()
                .toString() : "", ctx.getResponse()
                .getStatus(), userAgent, requestBody);

        return null;
    }

    private String hideCredentials(String requestBody) {
        if (requestBody.contains("password")) {
            String password = requestBody.substring(requestBody.indexOf("password") - 1)
                .split("\"")[3];
            requestBody = requestBody.replace(password, "***");
        }

        if (requestBody.contains("facebookToken")) {
            String facebookToken = requestBody.substring(requestBody.indexOf("facebookToken") - 1)
                .split("\"")[3];
            requestBody = requestBody.replace(facebookToken, "***");
        }
        if (requestBody.contains("newPassword")) {
            String facebookToken = requestBody.substring(requestBody.indexOf("newPassword") - 1)
                .split("\"")[3];
            requestBody = requestBody.replace(facebookToken, "***");
        }

        return requestBody;
    }

    private String getRequestBody(InputStream in) {
        String requestBody = null;
        try {
            requestBody = StreamUtils.copyToString(in, Charset.forName("UTF-8"));
            requestBody = requestBody.replaceAll(System.lineSeparator(), "");
        } catch (IOException e) {
            log.error("GATEWAY_EROOR_LOGGER_FILTER", e);
        }
        return requestBody;
    }
}

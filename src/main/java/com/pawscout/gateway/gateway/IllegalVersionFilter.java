package com.pawscout.gateway.gateway;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.pawscout.common.exception.ErrorType;
import com.pawscout.gateway.service.IllegalVersionService;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static com.netflix.zuul.context.RequestContext.getCurrentContext;

@Component
public class IllegalVersionFilter extends ZuulFilter {

    private static final String HEADER_USER_AGENT = "User-Agent";
    private final Logger log = LoggerFactory.getLogger(IllegalVersionFilter.class);
    private final IllegalVersionService illegalVersionService;

    @Autowired
    public IllegalVersionFilter(IllegalVersionService illegalVersionService) {
        this.illegalVersionService = illegalVersionService;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }


    @Override
    public boolean shouldFilter() {
        RequestContext ctx = getCurrentContext();
        String userAgent = ctx.getRequest()
            .getHeader(HEADER_USER_AGENT);

        if (log.isDebugEnabled()) {
            log.debug("User-Agent: {}", userAgent);
        }
        return Objects.nonNull(userAgent) && !illegalVersionService.isValid(userAgent);
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.setResponseStatusCode(ErrorType.ILLEGAL_VERSION.getHttpStatus()
            .value());
        try {
            ctx.setResponseBody(new JSONObject()
                .put("errorCode", ErrorType.ILLEGAL_VERSION.getCode())
                .put("message", ErrorType.ILLEGAL_VERSION.getMessage())
                .toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ctx.setSendZuulResponse(false);


        return null;
    }
}

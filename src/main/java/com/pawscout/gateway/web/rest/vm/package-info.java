/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pawscout.gateway.web.rest.vm;
